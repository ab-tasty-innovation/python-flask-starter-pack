FROM python:3.8.1-slim-buster

COPY . /app
WORKDIR /app

RUN pip install -r /app/requirements.txt

ENV FLASK_ENV "production"
ENV FLASK_APP "app.py"

EXPOSE 5000:5000

ENTRYPOINT [ "python" ]

CMD [ "app.py" ]
