#!/bin/bash

INCOMPLETE_INSTALL=false
REASONS="Installation failed because:"

PYTHON_PATH=$(exec which python3)
if [ $? -eq 1 ]; then
  INCOMPLETE_INSTALL=true
  REASONS+="\n- python3 is required"
fi

PIP_CHECK=$(exec python3 -m pip)
if [ $? -eq 1 ]; then
  INCOMPLETE_INSTALL=true
  REASONS+="\n- pip is required"
fi

VENV_CHECK=$(exec python3 -m virtualenv --version)
if [ $? -eq 1 ]; then
  INCOMPLETE_INSTALL=true
  REASONS+="\n- virtualenv is required"
fi

if [ "$INCOMPLETE_INSTALL" = false ]; then
  echo -e "Preparing virtual env:"
  python3 -m virtualenv -p "${PYTHON_PATH}" venv
  source venv/bin/activate
  echo -e "==========\nInstalling dependancies:"
  python3 -m pip install -r requirements.txt
  rm -rf .git/
  git init
  echo -e "==========\n\n==> Your flask environment is ready <==\nTo load your virtualenv type: source venv/bin/activate "
  exit 0
else
  echo -e "$REASONS"
  exit 1
fi
