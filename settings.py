import os

from dotenv import load_dotenv

load_dotenv()

ABASTY_API_URL = os.getenv("ABASTY_API_URL")

DEBUG = os.getenv("DEBUG")
HOST = os.getenv("HOST")
PORT = os.getenv("PORT")

DATABASE_HOST = os.environ.get("DATABASE_HOST")
DATABASE_NAME = os.environ.get("DATABASE_NAME")
DATABASE_USER = os.environ.get("DATABASE_USER")
DATABASE_PWD = os.environ.get("DATABASE_PWD")
