import requests

from settings import ABASTY_API_URL


class ABTastyClient:
    def __init__(self, url=None, token=None):
        self.url = url
        self.client = requests.Session()
        self.token = token
        if self.token:
            self.client.headers = {
                "Authorization": f"Bearer {token}",
                "Content-Type": "application/json",
            }

    def get(self, **kwargs):
        uri = kwargs.pop("uri")
        return self.client.get(self.url + uri, **kwargs)

    def create(self, **kwargs):
        uri = kwargs.pop("uri")
        return self.client.post(self.url + uri, **kwargs)

    def update(self, **kwargs):
        uri = kwargs.pop("uri")
        return self.client.put(self.url + uri, **kwargs)


def get_client_identifier(token, account_id):
    if not token or not account_id:
        return None
    client = ABTastyClient(url=ABASTY_API_URL, token=token)
    account = client.get(uri=f"/core/accounts/{account_id}").json()
    return account.get("identifier", None)
