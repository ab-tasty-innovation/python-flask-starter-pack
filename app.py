from functools import wraps

from flask import Flask, request

from abtasty_helper import get_client_identifier
from settings import DEBUG, HOST, PORT
from sql_handler import SQLHandler

app = Flask(__name__)


def login_required(f):  # middleware for auth every request
    @wraps(f)
    def decorated_function(*args, **kwargs):
        account_id = request.headers.get("X-Abtasty-Account")
        token = request.headers.get("Authorization")

        client_authorized = get_client_identifier(token, account_id)

        if not client_authorized:
            return (
                {
                    "error": "access_denied",
                    "error_description": "OAuth2 authentication required",
                },
                403,
            )
        return f(*args, **kwargs)

    return decorated_function


@app.route("/")
def hello_world():
    return {"message": "Hello World!"}, 200


@app.route("/auth")
@login_required
def hello_world_auth():
    return {"message": "Hello World!"}, 200


if __name__ == "__main__":
    app.run(debug=DEBUG, host=HOST, port=int(PORT))
