

<br>
<img src="https://app2.abtasty.com/src/components/turfu/turfuHeader/assets/abtasty-logo36fbbd1937f55ed4194361d3bfca208f.png"  width="200" height="40" style="float: right;">
<br>


# Python - Flask starter pack

Boilerplate for generate a project for an API with Flask.

Project Structure
--------

  ```
  ├── abtasty_helper.py
  ├── app.py
  ├── README.md
  ├── Dockerfile
  ├── settings.py
  ├── .gitlab-ci.yml
  ├── sql_handler.py
  └── requirements.txt
  ```


### Quick Start

1. Clone the repo
  ```
  $ git clone https://gitlab.com/ab-tasty-innovation/python-flask-starter-pack.git
  $ cd python-flask-starter-pack
  ```

2. Setup your virtualenv and install the dependencies:
  ```
  $ chmod +x install.sh
  $ ./install.sh
  ```

3. Fill the .env file with your need:

4. Run the development server:
  ```
  $ python app.py
  ```

5. Navigate to [http://localhost:5000](http://localhost:5000)

### Docker install

1. Build the image:
  ```
  $  docker build -t boilerplate-python-api .
  ```

2. Run the image:
  ```
  $ docker run -it --rm -p 5000:5000 boilerplate-python-api
  ```
3. Fill the .env file with your need:

4. Navigate to [http://localhost:5000](http://localhost:5000)

## Database
You can use `sql_handler.py` for interacting with the database. You have to instantiate the class with your credentials. You can use `_connect` method for initiate a connection with your database.

Execute your SQL query with `execute_query` method


## Code style

### Sort import
If you want to sort correctly all your import you can execute this commands. Please refer to the documentation for [more details.](https://pypi.org/project/isort/)
  ```
  $  isort -rc .
  ```

### Lint
If you want to lint and format correctly. Please refer to the documentation for [more details.](https://pypi.org/project/black/)
  ```
  $  black .
  ```
## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## Author

Agostin Jean-Baptiste <[jean-baptiste.agostin@abtasty.com](mailto:jean-baptiste.agostin@abtasty.com)>

[AB Tasty](https://www.abtasty.com/)