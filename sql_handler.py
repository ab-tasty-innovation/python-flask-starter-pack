import pymysql
from pymysql import OperationalError


class SQLHandler:
    def __init__(self, host, username, password, db_name):
        self.host = host
        self.username = username
        self.password = password
        self.db_name = db_name
        self.connection = None

    def _connect(self):
        if self.connection is None:
            self.connection = pymysql.connect(
                host=self.host,
                user=self.username,
                password=self.password,
                db=self.db_name,
                cursorclass=pymysql.cursors.DictCursor,
            )
            return self.connection
        return self.connection

    def execute_query(self, query):
        connection = self._connect()
        try:
            with connection.cursor() as cursor:
                cursor.execute(query)
                return cursor
        finally:
            connection.close()
